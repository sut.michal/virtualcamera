from tkinter import Canvas
from typing import List
from geometry.objects import Cuboid


class Scene:
    """
    Scene is a set of objects
    """
    def __init__(self, objects: List[Cuboid], d=10, center=(0, 0)):
        self.objects = objects
        self.d = d
        self.center = center

    def draw(self, canvas: Canvas) -> None:
        canvas.delete('all')
        for _object in self.objects:
            _object.draw_to_canvas(canvas, d=self.d, offset=self.center)

    def camera_move(self, r: int=0, l: int=0, u: int=0, d: int=0, f: int=0, b: int=0) -> None:
        for _object in self.objects:
            _object.move(-r, -l, -u, -d, -f, -b)

    def camera_rotate(self, y_deg: float=0, x_deg: float=0, z_deg: float=0) -> None:
        for _object in self.objects:
            _object.rotate(-y_deg, -x_deg, -z_deg)

    # ----==== Helper functions ====----
    def camera_right(self, step: int) -> None:
        self.camera_move(r=step)

    def camera_left(self, step: int) -> None:
        self.camera_move(l=step)

    def camera_up(self, step: int) -> None:
        self.camera_move(u=step)

    def camera_down(self, step: int) -> None:
        self.camera_move(d=step)

    def camera_forward(self, step: int) -> None:
        self.camera_move(f=step)

    def camera_backward(self, step: int) -> None:
        self.camera_move(b=step)

    def camera_rotate_y(self, deg: float) -> None:
        self.camera_rotate(y_deg=deg)

    def camera_rotate_x(self, deg: float) -> None:
        self.camera_rotate(x_deg=deg)

    def camera_rotate_z(self, deg: float) -> None:
        self.camera_rotate(z_deg=deg)

    def camera_zoom(self, z):
        self.d += z
