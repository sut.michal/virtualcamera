from tkinter import Tk, Canvas, Frame, BOTH
from typing import Optional
from scene import Scene


class MainWindow(Frame):
    def __init__(self, master=None, scene: Optional[Scene]=None, *args, **kwargs):
        super().__init__(master=master, *args, **kwargs)
        self.canvas = Canvas(self, bg='black')
        self.scene = scene
        self.initUI()

    def initUI(self):
        self.master.title("Virtual Camera")
        self.pack(fill=BOTH, expand=1)
        self.canvas.pack(fill=BOTH, expand=1)
        self.scene.draw(canvas=self.canvas)

    def view_move(self, x, y):
        self.scene.center = self.scene.center[0] + x, self.scene.center[1] + y
        self.scene.draw(self.canvas)

    def camera_right(self, step: int):
        self.scene.camera_right(step)
        self.scene.draw(self.canvas)

    def camera_left(self, step: int):
        self.scene.camera_left(step)
        self.scene.draw(self.canvas)

    def camera_up(self, step: int):
        self.scene.camera_up(step)
        self.scene.draw(self.canvas)

    def camera_down(self, step: int):
        self.scene.camera_down(step)
        self.scene.draw(self.canvas)

    def camera_forward(self, step: int):
        self.scene.camera_forward(step)
        self.scene.draw(self.canvas)

    def camera_backward(self, step: int):
        self.scene.camera_backward(step)
        self.scene.draw(self.canvas)

    def camera_rotate_y(self, deg: float):
        self.scene.camera_rotate_y(deg)
        self.scene.draw(self.canvas)

    def camera_rotate_x(self, deg: float):
        self.scene.camera_rotate_x(deg)
        self.scene.draw(self.canvas)

    def camera_rotate_z(self, deg: float):
        self.scene.camera_rotate_z(deg)
        self.scene.draw(self.canvas)

    def camera_zoom(self, z):
        self.scene.camera_zoom(z)
        self.scene.draw(self.canvas)
