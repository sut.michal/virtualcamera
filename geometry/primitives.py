class Point:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

    def __str__(self):
        return f"({self.x}, {self.y}, {self.z})"

    def __repr__(self):
        return str(self)

    def __hash__(self):
        return hash((self.x, self.y, self.z))

    def __eq__(self, other):
        return isinstance(other, Point) and other.x == self.x and other.y == self.y and other.z == self.z

    def to_tuple(self):
        return self.x, self.y, self.z

    def deconstruct(self):
        return self.x, self.y, self.z


class Line:
    def __init__(self, a: Point, b: Point):
        self.a = a
        self.b = b

        self.x1 = a.x
        self.y1 = a.y
        self.z1 = a.z

        self.x2 = b.x
        self.y2 = b.y
        self.z2 = b.z

    def to_tuple(self):
        return self.x1, self.y1, self.z1, self.x2, self.y2, self.z2

    def __str__(self):
        return str(self.to_tuple())

    def __repr__(self):
        return str(self)

    def __iter__(self):
        yield self.x1
        yield self.y1
        yield self.z1
        yield self.x2
        yield self.y2
        yield self.z2
