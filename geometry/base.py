from operations import move, rotate
from operations import perspective_view
from tkinter import Canvas


class BaseObject:
    def __init__(self, dimensions: tuple, start_point: tuple=(0, 0, 0),
                 vertices_color: str= 'red', edges_color: str= 'white', vertex_size=3, edge_size=2):
        self.voxels_color = vertices_color
        self.edges_color = edges_color
        self.start_point = start_point
        self.dimensions = dimensions
        self.voxel_size = vertex_size
        self.edge_size = edge_size
        self.points = self._build_points()

    def _build_points(self):
        """
        Must be overridden by subclasses
        :raise: NotImplementedError
        :return: list of Points
        """
        raise NotImplementedError

    def _build_edges(self, points):
        """
        Must be overridden by subclasses
        :param points: Optional list of points to use to build edges. Uses self.points if it's None
        :raise: NotImplementedError
        :return: list of edges (list of Line objects)
        """
        raise NotImplementedError

    def move(self, r=0, l=0, u=0, d=0, f=0, b=0):
        self.points = [move(point, r, l, u, d, f, b) for point in self.points]

    def rotate(self, y_deg=0, x_deg=0, z_deg=0):
        self.points = [rotate(point, y_deg, x_deg, z_deg) for point in self.points]

    def get_projected_points(self, d):
        return [perspective_view(point, d) for point in self.points]

    def get_edges_2d(self, projected_points) -> list:
        """
        Return list of Lines after applying perspective projection.
        :return: list of Line objects
        """
        return self._build_edges(projected_points)

    def draw_to_canvas(self, canvas: Canvas, d=10, offset=(200, 200)):
        projected_points = self.get_projected_points(d)
        for i, line in enumerate(self.get_edges_2d(projected_points)):
            x1, y1, z1, x2, y2, z2 = line
            ec = self.edges_color
            canvas.create_line(x1+offset[0], y1+offset[1], x2+offset[0], y2+offset[1], fill=ec, width=self.edge_size)
        for point in projected_points:
            canvas.create_oval(point.x+offset[0], point.y+offset[1], point.x+offset[0], point.y+offset[1],
                               outline=self.edges_color, width=self.voxel_size)
