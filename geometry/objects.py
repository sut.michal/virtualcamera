from .primitives import Point, Line
import math as m
from .base import BaseObject

r"""
Order of points in cuboid:

       +------------------+
      /|6                /|7
     / |                / |
    +------------------+  |
    |4 |               |5 |
    |  |               |  |
    |  |               |  |
    |  |               |  |
    |  |               |  |
    |  |               |  |
    |  |2              |  |3
    |  +------------------+
    | /                | /
    |/                 |/
    +------------------+
    0                   1


Order of points in tetrahedron:
1. bottom, left, front
2. bottom, right, front,
3. bottom, middle, back,
4. top (peak)
"""


class Cuboid(BaseObject):
    def _build_points(self):
        point = Point(*self.start_point)
        x, y, z = point.x, point.y, point.z
        x_dim, y_dim, z_dim = self.dimensions
        return [
            Point(x, y, z), Point(x + x_dim, y, z), Point(x, y, z + z_dim), Point(x + x_dim, y, z + z_dim),  # bottom
            Point(x, y - y_dim, z), Point(x + x_dim, y - y_dim, z), Point(x, y - y_dim, z + z_dim), Point(x + x_dim, y - y_dim, z + z_dim)  # top
        ]

    def _build_edges(self, points=None):
        p = points or self.points
        L = Line
        return [
            L(p[0], p[1]), L(p[0], p[2]), L(p[1], p[3]), L(p[2], p[3]),
            L(p[0], p[4]), L(p[1], p[5]), L(p[2], p[6]), L(p[3], p[7]),
            L(p[4], p[5]), L(p[4], p[6]), L(p[5], p[7]), L(p[6], p[7]),
        ]


class Tetrahedron(BaseObject):
    def _build_points(self):
        P = Point
        point = P(*self.start_point)
        x, y, z = point.x, point.y, point.z
        a = self.dimensions[0]
        height = m.sqrt(6)/3 * a
        wall_height = (a * m.sqrt(3))/2
        return [
            P(x, y, z), P(x + a, y, z), P(x + a/2, y, z+wall_height),
            P(x + a/2, y-height, z+(1/3)*wall_height),
        ]

    def _build_edges(self, points=None):
        p = points or self.points
        L = Line
        return [
            L(p[0], p[1]), L(p[0], p[2]), L(p[1], p[2]),
            L(p[0], p[3]), L(p[1], p[3]), L(p[2], p[3]),
        ]
