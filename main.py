from tkinter import Tk
from scene_components import *
from scene import Scene
from window import MainWindow
from settings import *


def main():
    root = Tk()

    objects = [
        cuboid1,
        cuboid2,
        cuboid3,
        cuboid4,
        cuboid5,
        cuboid6,
        tetrahedron1
    ]

    scene = Scene(objects=objects, d=INITIAL_D, center=(WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2))
    ex = MainWindow(root, scene=scene)

    root.geometry(f"{WINDOW_WIDTH}x{WINDOW_HEIGHT}")
    root.bind("<Control-Right>", lambda e: ex.camera_rotate_y(Y_ROTATION_STEP))
    root.bind("<Control-Left>", lambda e: ex.camera_rotate_y(-Y_ROTATION_STEP))
    root.bind("<Control-Up>", lambda e: ex.camera_rotate_x(X_ROTATION_STEP))
    root.bind("<Control-Down>", lambda e: ex.camera_rotate_x(-X_ROTATION_STEP))
    root.bind("<Control-z>", lambda e: ex.camera_rotate_z(-Z_ROTATION_STEP))
    root.bind("<Control-x>", lambda e: ex.camera_rotate_z(Z_ROTATION_STEP))
    root.bind("<Shift-Left>", lambda e: ex.view_move(VIEW_Y_MOVE_STEP, 0))
    root.bind("<Shift-Right>", lambda e: ex.view_move(-VIEW_Y_MOVE_STEP, 0))
    root.bind("<Shift-Up>", lambda e: ex.view_move(0, VIEW_X_MOVE_STEP))
    root.bind("<Shift-Down>", lambda e: ex.view_move(0, -VIEW_X_MOVE_STEP))
    root.bind("<Right>", lambda e: ex.camera_right(MOVE_RIGHT_STEP))
    root.bind("<Left>", lambda e: ex.camera_left(MOVE_LEFT_STEP))
    root.bind("<Down>", lambda e: ex.camera_backward(MOVE_BACKWARD_STEP))
    root.bind("<Up>", lambda e: ex.camera_forward(MOVE_FORWARD_STEP))
    root.bind("<Prior>", lambda e: ex.camera_up(MOVE_UP_STEP))
    root.bind("<Next>", lambda e: ex.camera_down(MOVE_DOWN_STEP))
    root.bind('<Control-Prior>', lambda e: ex.camera_zoom(ZOOM_STEP))
    root.bind('<Control-Next>', lambda e: ex.camera_zoom(-ZOOM_STEP))

    root.mainloop()


if __name__ == '__main__':
    main()
