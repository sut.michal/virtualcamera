from geometry.objects import *
offset = 100
# Road
cuboid1 = Cuboid(dimensions=(80, 80, 80), start_point=(-130, -40, 40), vertices_color='white', edges_color='yellow')
cuboid2 = Cuboid(dimensions=(80, 80, 80), start_point=(-40, -40, 40), vertices_color='white', edges_color='green')
cuboid3 = Cuboid(dimensions=(80, 80, 80), start_point=(50, -40, 40), vertices_color='white', edges_color='blue')
cuboid4 = Cuboid(dimensions=(80, 80, 80), start_point=(-85, 50, 40), vertices_color='white', edges_color='pink')
cuboid5 = Cuboid(dimensions=(80, 80, 80), start_point=(5, 50, 40), vertices_color='white', edges_color='cyan')
cuboid6 = Cuboid(dimensions=(80, 80, 80), start_point=(-40, 140, 40), vertices_color='white', edges_color='white')

tetrahedron1 = Tetrahedron(dimensions=(80,), start_point=(-40, -130, 40), edges_color='SlateBlue1')


