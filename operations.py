import numpy as np
import math as m
from geometry.primitives import Point


def y_rotation_matrix(angle):
    angle = m.radians(angle)
    return np.array([
        [m.cos(angle),  0, m.sin(angle), 0],
        [0,             1, 0,            0],
        [-m.sin(angle), 0, m.cos(angle), 0],
        [0,             0, 0,            1]
    ], dtype='float32')


def x_rotation_matrix(angle):
    angle = m.radians(angle)
    return np.array([
        [1, 0,            0,             0],
        [0, m.cos(angle), -m.sin(angle), 0],
        [0, m.sin(angle), m.cos(angle),  0],
        [0, 0,            0,             1],
    ], dtype='float32')


def z_rotation_matrix(angle):
    angle = m.radians(angle)
    return np.array([
        [m.cos(angle), -m.sin(angle), 0, 0],
        [m.sin(angle), m.cos(angle),  0, 0],
        [0,            0,             1, 0],
        [0,            0,             0, 1]
    ])


def rotate(point, y_deg, x_deg, z_deg):
    y_rotation = y_rotation_matrix(y_deg)
    y_x_rotation = np.matmul(y_rotation, x_rotation_matrix(x_deg))
    y_x_z_rotation = np.matmul(y_x_rotation, z_rotation_matrix(z_deg))
    point = list(np.matmul(y_x_z_rotation, np.array([point.x, point.y, point.z, 1])).tolist())
    return Point(point[0], point[1], point[2])


def perspective_view(point, d):
    x, y, z = point.deconstruct()
    return Point((x * d)/z, (y * d)/z, (z*d)/z)


def move(point, right=0, left=0, up=0, down=0, forward=0, backward=0):
    return Point(point.x + right - left, point.y - up + down, point.z + forward - backward)
